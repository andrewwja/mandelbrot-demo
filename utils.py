import numpy
import cv2

def create_input_array(x1, x2, y1, y2, width, height):
  xx = numpy.arange(x1, x2, (x2-x1)/width)
  yy = numpy.arange(y2, y1, (y1-y2)/height) * 1j
  q = numpy.ravel(xx+yy[:, numpy.newaxis]).astype(numpy.complex64)
  return q.reshape(height, width)

def write_image_file(filename, array):
  cv2.imwrite(filename, cv2.merge((array.astype(numpy.uint8),
                                   numpy.zeros_like(array, dtype=numpy.uint8),
                                   array.astype(numpy.uint8)
                                   )))

def brighten_image(image, factor):
  bright = image.astype(numpy.double)
  bright = numpy.clip(bright * (256/factor), 0, 256)
  return bright.astype(numpy.uint8)
