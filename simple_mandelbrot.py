#!/usr/bin/env python3
import utils
import numpy
import time

def mandelbrot(input_array, max_iterations):
  z = numpy.zeros_like(input_array, numpy.complex64)
  output_array = numpy.zeros_like(input_array, dtype=numpy.uint8)

  width = input_array.shape[1]
  height = input_array.shape[0]

#  for iteration in range(max_iterations):
#    for y in range(height):
#      for x in range(width):
#        z[y][x] = z[y][x] * z[y][x] + input_array[y][x]
#
#        if abs(z[y][x]) > 2.0: # the point escapes the set
#          input_array[y][x] = 0+0j
#          z[y][x] = 0+0j
#          output_array[y][x] = iteration

  for iteration in range(max_iterations):
    z = z*z + input_array
    done = numpy.greater(abs(z), 2.0)
    input_array = numpy.where(done, 0+0j, input_array)
    z = numpy.where(done, 0+0j, z)
    output_array = numpy.where(done, iteration, output_array)

  return output_array

if __name__ == '__main__':
  zoom = 15
  centre_x = -(1.2 * zoom)
  centre_y = 0
  width = 3840
  height = 2160
  max_iterations = 84

  input_array = utils.create_input_array(
                                          (centre_x-(width/100.0))/zoom,
                                          (centre_x+(width/100.0))/zoom,
                                          (centre_y-(height/100.0))/zoom,
                                          (centre_y+(height/100.0))/zoom,
                                          width, height
                                        )

  start = time.time()
  output_array = mandelbrot(input_array, max_iterations)
  end = time.time()

  print("mandelbrot {}x{} took {}s".format(width, height, end-start))

  output_array = utils.brighten_image(output_array, max_iterations)

  utils.write_image_file("simple.bmp", output_array)
