#!/usr/bin/env python3
import utils
import numpy
import time
import threading

def mandelbrot(input_chunks, output_chunks, max_iterations, index):
  input_array = input_chunks[index]
  z = numpy.zeros_like(input_array, numpy.complex64)
  output_array = numpy.zeros_like(input_array, dtype=numpy.uint8)

  width = input_array.shape[1]
  height = input_array.shape[0]

#  for iteration in range(max_iterations):
#    for y in range(height):
#      for x in range(width):
#        z[y][x] = z[y][x] * z[y][x] + input_array[y][x]
#
#        if abs(z[y][x]) > 2.0: # the point escapes the set
#          input_array[y][x] = 0+0j
#          z[y][x] = 0+0j
#          output_array[y][x] = iteration

  for iteration in range(max_iterations):
    z = z*z + input_array
    done = numpy.greater(abs(z), 2.0)
    input_array = numpy.where(done, 0+0j, input_array)
    z = numpy.where(done, 0+0j, z)
    output_array = numpy.where(done, iteration, output_array)

  output_chunks[index] = output_array

if __name__ == '__main__':
  zoom = 15
  centre_x = -(1.2 * zoom)
  centre_y = 0
  width = 3840
  height = 2160
  max_iterations = 84

  input_array = utils.create_input_array(
                                          (centre_x-(width/100.0))/zoom,
                                          (centre_x+(width/100.0))/zoom,
                                          (centre_y-(height/100.0))/zoom,
                                          (centre_y+(height/100.0))/zoom,
                                          width, height
                                        )

  # break the work up into chunks on the Y axis
  number_of_chunks = 16
  chunk_height = int(height / number_of_chunks)
  chunk_width = width

  input_chunks = []
  threads = []
  output_chunks = []

  for i in range(number_of_chunks):
    start_y = i * chunk_height
    end_y = start_y + chunk_height
    start_x = 0
    end_x = chunk_width
    input_chunks.append(input_array[start_y:end_y, start_x:end_x])
    output_chunks.append(numpy.zeros_like(input_chunks, dtype=numpy.uint8))
    print("Created input chunk {}: pixels ({},{}) to ({},{}) ({}x{})".format(i, start_y, start_x, end_y, end_x, input_chunks[i].shape[0], input_chunks[i].shape[1]))

  # create all the threads
  for i in range(number_of_chunks):
    print("Created thread {}".format(i))
    threads.append(threading.Thread(target=mandelbrot, args=(input_chunks, output_chunks, max_iterations, i,)))

  start = time.time()

  # start all the threads
  for i in range(number_of_chunks):
    print("Starting thread {}".format(i))
    threads[i].start()

  print("Waiting for threads to finish")

  # wait for each thread to stop
  for i in range(number_of_chunks):
    threads[i].join()

  end = time.time()

  print("mandelbrot {}x{} took {}s".format(width, height, end-start))

  output_array = numpy.concatenate(output_chunks)

  output_array = utils.brighten_image(output_array, max_iterations)

  utils.write_image_file("threaded.bmp", output_array)
