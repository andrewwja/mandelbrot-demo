#!/usr/bin/env python3
import utils
import numpy
import time
import threading
import pyopencl as cl

def mandelbrot(input_chunks_in_RAM, output_chunks_in_RAM, max_iterations):
  # List all the devices in this computer
  platforms = cl.get_platforms()

  for platform in platforms:
    print("Found a device: {}".format(str(platform)))

  platform = 1 # You will almost certainly have to change this

  ctx = cl.Context(dev_type=cl.device_type.ALL,
                   properties=[(cl.context_properties.PLATFORM, platforms[platform])])

  # Create a command queue on the platform (device = None means OpenCL picks a device for us)
  queue = cl.CommandQueue(ctx, device = None)

  # This is our OpenCL shader. It does a single point (OpenCL is responsible for mapping it across all the points in a chunk)
  code = cl.Program(ctx, """
  #pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable
  __kernel void mandelbrot(__global float2 *input, __global uchar *output, ushort const width, uchar const maxiter)
  {
    int gidY = get_global_id(0);
    int gidX = get_global_id(1);

    float cx = input[(width * gidY) + gidX].x;
    float cy = input[(width * gidY) + gidX].y;

    float x = 0.0f;
    float y = 0.0f;
    uchar iteration = 0;

    while (((x*x + y*y) < 4.0f) && (iteration < maxiter)) {
      float xtemp = x*x - y*y + cx;
      y = 2*x*y + cy;
      x = xtemp;
      iteration++;
    }

    if (iteration == maxiter) {
      output[(width * gidY) + gidX] = (uchar)0;
    } else {
      output[(width * gidY) + gidX] = iteration;
    }
  }
  """).build()

  output_chunks_on_GPU = []
  output_chunk_shapes = []
  shader = code.mandelbrot
  start = time.time()

  for input_chunk_in_RAM in input_chunks_in_RAM:
    chunk_shape = input_chunk_in_RAM.shape

    # These are our buffers to hold data on the device
    input_chunk_on_GPU = cl.Buffer(ctx, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=input_chunk_in_RAM)

    output_chunk_on_GPU = cl.Buffer(ctx, cl.mem_flags.WRITE_ONLY, int(chunk_shape[0] * chunk_shape[1]))

    # Call the shader on this chunk
    shader.set_args(input_chunk_on_GPU, output_chunk_on_GPU, numpy.uint16(chunk_shape[1]), numpy.uint8(max_iterations))
    cl.enqueue_nd_range_kernel(queue, shader, chunk_shape, None)

    # Add the output chunk to our list to keep track of it
    output_chunks_on_GPU.append(output_chunk_on_GPU)
    output_chunk_shapes.append(chunk_shape)

  # Wait for all the chunks to be computed
  queue.finish()

  for i in range(len(output_chunks_on_GPU)):
    output_chunks_in_RAM[i] = numpy.zeros_like(input_chunks_in_RAM[i], dtype=numpy.uint8)

    # Copy the GPU data back to RAM
    cl.enqueue_copy(queue, output_chunks_in_RAM[i], output_chunks_on_GPU[i]).wait()

  end = time.time()
  return (start, end)

if __name__ == '__main__':
  zoom = 15
  centre_x = -(1.2 * zoom)
  centre_y = 0
  width = 3840
  height = 2160
  max_iterations = 84

  input_array = utils.create_input_array(
                                          (centre_x-(width/100.0))/zoom,
                                          (centre_x+(width/100.0))/zoom,
                                          (centre_y-(height/100.0))/zoom,
                                          (centre_y+(height/100.0))/zoom,
                                          width, height
                                        )

  # break the work up into chunks on the Y axis
  number_of_chunks = 1
  chunk_height = int(height / number_of_chunks)
  chunk_width = width

  input_chunks = []
  output_chunks = []

  for i in range(number_of_chunks):
    start_y = i * chunk_height
    end_y = start_y + chunk_height
    start_x = 0
    end_x = chunk_width
    input_chunks.append(input_array[start_y:end_y, start_x:end_x])
    output_chunks.append(numpy.zeros_like(input_chunks, dtype=numpy.uint8))
    print("Created input chunk {}: pixels ({},{}) to ({},{}) ({}x{})".format(i, start_y, start_x, end_y, end_x, input_chunks[i].shape[0], input_chunks[i].shape[1]))

  (start, end) = mandelbrot(input_chunks, output_chunks, max_iterations)

  print("mandelbrot {}x{} took {}s".format(width, height, end-start))

  output_array = numpy.concatenate(output_chunks)

  output_array = utils.brighten_image(output_array, max_iterations)

  utils.write_image_file("gpu.bmp", output_array)
